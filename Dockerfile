# alpine repo/image is multiarch or architecture-cognitive so
# no specific arch-flags /paths needed
FROM alpine:3.15.4
ARG RELEASE="1.21.0"

RUN echo 'syncthing:x:1000:1000::/var/syncthing:/sbin/nologin' >> /etc/passwd \
    && echo 'syncthing:!::0:::::' >> /etc/shadow \
    && mkdir /var/syncthing \
    && chown syncthing /var/syncthing

RUN apk --update --no-cache add ca-certificates \
    && mkdir -p /tmp/install /bin \
    && cd /tmp/install \
    && wget -qO- https://github.com/syncthing/syncthing/releases/download/v$RELEASE/syncthing-linux-arm64-v$RELEASE.tar.gz |tar xz \
    && cp /tmp/install/syncthing-linux-arm64-v$RELEASE/syncthing /bin \
    && chown 1000:1000 /bin/syncthing \
    && rm -fR /tmp/install

USER syncthing
ENV STNOUPGRADE=1

HEALTHCHECK --interval=1m --timeout=10s \
  CMD nc -z localhost 8384 || exit 1
ENTRYPOINT ["/bin/syncthing", "-home", "/var/syncthing/config", "-gui-address", "0.0.0.0:8384"]

