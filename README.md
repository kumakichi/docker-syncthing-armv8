# regarding the UDP error at startup
```
syncthing    | 2022/06/06 12:48:31 failed to sufficiently increase receive buffer size (was: 208 kiB, wanted: 2048 kiB, got: 416 kiB). See https://github.com/lucas-clemente/quic-go/wiki/UDP-Receive-Buffer-Size for details.

```
You might want to edit the sysctl in the alpine base image which is not working as the key is nonexistent.
Instead, edit the sysctls for your host, running the container.


On my Ubuntu 20.04. on an odroid hc-4 the values are as follows:
```
root@xander:~# sysctl -a | grep rmem
net.core.rmem_default = 212992
net.core.rmem_max = 212992
```
which is the limit you see in the error message.

So set a /etc/sysctl.conf file with sth like this and reboot or write the value directly via sysctl -w <key-name>=<value>
```
net.core.rmem_default = 524288    # 512 KiB
net.core.rmem_max = 2097152  # 2MiB
```

live, for testing (won'T survive a reboot)
```
root@xander:~# sysctl -w net.ipv4.tcp_rmem=524288
net.ipv4.tcp_rmem = 524288
root@xander:~# sysctl -w net.core.rmem_max=2097152
net.core.rmem_max = 2097152
root@xander:~# sysctl -a | grep rmem
net.core.rmem_default = 212992
net.core.rmem_max = 2097152
net.ipv4.tcp_rmem = 524288	87380	6291456
net.ipv4.udp_rmem_min = 4096
root@xander:~# 
```
